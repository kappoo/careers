<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    //

    protected  $fillable=[
        'subject','name','cv','email','is_read'
    ];
}
