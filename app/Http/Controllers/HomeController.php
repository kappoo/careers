<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function postCv(Request $request)
    {
        $this->validate($request,[

            'name'=>'required|between:2,250',
            'email'=>'required|email',
            'subject'=>'required|between:2,100',
                'cv'=>'required|file|mimes:pdf,docx'

        ]);


        $file=$this->upload_file($request->file('cv'),'files');
        \App\Request::create(array_merge(array_except($request->all(),['cv']),['cv'=>$file]));


        Session::put('success','cv uploaded successfully');

        return back()->withInput($request->all());
    }



    function upload_file( $file, $module ) {


        $random_name = $this->create_random_name();

        $extension   = $file->getClientOriginalExtension();
        $file_name    =  $random_name . '.' . $extension;

        if ( $file->move( public_path()  .'/'.$module , $file_name ) ) {
            return $file_name;
        }

    }


    function create_random_name() {

        $time       = time();
        $random     = rand( 1, 100000 );
        $divide     = $time / $random;
        $encryption = md5( $divide );
        $name_enc   = substr( $encryption, 0, 20 );

        return $name_enc;
    }



}
