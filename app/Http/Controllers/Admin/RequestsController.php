<?php

namespace App\Http\Controllers\Admin;


use App\Course;
use App\CourseAnnouncement;
use App\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Language;

class RequestsController extends Controller
{


    public function __construct()    {


    }


    public function index(Request $request){

        // set limit in request or default will be 12
        $limit = $request->limit?(int)$request->limit:12 ;
        // set date order in request or default will be ascending
        $date_order = $request->date_order?$request->date_order:"desc" ;

        $requests = \App\Request::query();
        $search = $request->search ;
        if ($request->search)
        {
            // if search appended then filter with search arabic name
            // order data with date and return it with pagination
            $requests = $requests
                ->where('email', 'LIKE', "%$request->search%")
                ->orWhere('subject', 'LIKE', "%$request->search%")
                ->orWhere('name','LIKE',"%$request->search%")
                ->orderBy('is_read', 'asc')
                ->orderBy('created_at', $request->date_order)
                ->paginate($limit) ;
        }else{
            // order data with date and return it with pagination
            $requests = $requests
                ->orderBy('is_read', 'asc')
                ->orderBy('created_at', $request->date_order)
                ->paginate($limit) ;
        }




        $count=\App\Request::count();
        $unseen=\App\Request::where('is_read',0)->count();

        return view("admin.requests.index" , compact("requests" , "search",'count','unseen'));
    }


    public function create()
    {
        $languages = Language::where("is_active" , 1)->pluck("locale" , "name");


        $courses=Course::where('is_active',1)->pluck('title','id');



        return view('admin/announcements/create',compact('languages','courses') );
    }







    public function show($id)
    {
       $message=\App\Request::where('id',$id)->first();


        \App\Request::where('id',$id)->update(['is_read'=>1]);

       return view('admin.requests.show',compact('message'));

    }



  
    public function destroy($id)
    {



        try {


            $cv=  \App\Request::where('id',$id)->first();
            \File::delete(public_path().'/files/'.$cv->cv);

            \App\Request::where('id',$id)->delete();

        } catch (Exception $e) {
            // Error in process
            Session::put('error', 'Error in deleting this record');
        }
        Session::put('success', 'Cv  deleted successfully');
        return back();
    }








}
