@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('error'))

    <div class="alert alert-danger">
        {{ session('error') }}
        <?php Session::forget('error'); ?>
    </div>
@endif

@if (session('success'))

    <div class="alert alert-success">
        {{ session('success') }}
        <?php Session::forget('success'); ?>
    </div>
@endif

@if (session('info'))

    <div class="alert alert-info">
        {{ session('info') }}
        <?php Session::forget('info'); ?>
    </div>
@endif

@if (session('info2'))

    <div class="alert alert-info">
        {{ session('info2') }}
        <?php Session::forget('info2'); ?>
    </div>
@endif