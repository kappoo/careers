<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">



        <!-- Styles -->

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content" style="margin: 20px">

                @include('partials._form-errors')

                <div class="col-md-6">
                    <h3 class="title">Send your Cv</h3>

                    <form action="/post-cv" method="post" class="ol-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="full name *">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="email *">
                                </div>
                            </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control" placeholder="subject *">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="cv" class="form-control" >
                            </div>
                        </div>

                        <button type="submit" class="btn btn-danger btn-lg">submit</button>
                        <br/>

                    </form>

                    <br/>
                </div>

            </div>
        </div>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
