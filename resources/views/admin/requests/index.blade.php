@extends('admin/layout')

@section('styles')

@stop

@section('content')
    <article class="content items-list-page">
        @include('partials._form-errors')
        <div class="title-search-block">
            <div class="title-block">
                <div class="col-md-6">
                    <h3 class="title"> Contact

                    </h3>
                    <p class="title-description">Total Requests : {{$count}}</p>
                </div>

                <div class="col-md-6">

                    <p class="title-description">un seen requets : {{$unseen}}</p>
                </div>
            </div>

            <div class="items-search">
                <form class="form-inline" action="/admin/requests" method="get" >

                    <div class="input-group">
                        <input name="search" type="text" value="{{@$search}}" class="form-control boxed rounded-s" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-secondary rounded-s" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin">Home</a></li>
            <li class="active">Job request</li>

        </ol>

        @if(count($requests))
            <div class="card items">
                <ul class="item-list striped">
                    <li class="item item-list-header hidden-sm-down">
                        <div class="item-row">
                            <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
                                    <input type="checkbox" class="checkbox">
                                    <span></span>
                                </label> </div>


                            <div class="item-col item-col-header fixed item-col-img md">
                                <div> <span>ID</span> </div>
                            </div>

                            <div class="item-col item-col-header item-col-title ">
                                <div> <span>Name</span> </div>
                            </div>


                            <div class="item-col item-col-header item-col-title ">
                                <div class="no-overflow"> <span>Email</span> </div>
                            </div>


                            <div class="item-col item-col-header item-col-title ">
                                <div class="no-overflow"> <span>Subject</span> </div>
                            </div>





                            <div class="item-col item-col-header item-col-title ">
                                <div class="no-overflow"> <span>seen</span> </div>
                            </div>



                            <div class="item-col item-col-header item-col-title ">
                                <div> <span>Created</span> </div>
                            </div>

                            <div class="item-col item-col-header fixed item-col-actions-dropdown"> </div>
                        </div>
                    </li>
                    @foreach($requests as $item)
                        <li class="item @if(!$item->is_read) shadow @endif">
                            <div class="item-row">

                                <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
                                        <input type="checkbox" class="checkbox">
                                        <span></span>
                                    </label> </div>



                                <div class="item-col fixed item-col-img md">
                                    <div class="item-heading">ID</div>
                                    <div>
                                        <a href="{{ URL('/admin/contactus/' . $item->id ) }}" class="">
                                            <h4 class="item-title"> {{ @$item->id }} </h4>
                                        </a>
                                    </div>
                                </div>

                                <div class="item-col fixed pull-left item-col-title">
                                    <div class="item-heading">Course</div>
                                    <div>
                                        <a href="{{ URL('/admin/requests/' . $item->id ) }}" class="">
                                            <h4 class="item-title">

                                                {{ @$item->name}}
                                            </h4>
                                        </a>
                                    </div>
                                </div>

                                <div class="item-col fixed pull-left item-col-title">
                                    <div class="item-heading">Course</div>
                                    <div>
                                        <a href="{{ URL('/admin/requests/' . $item->id ) }}" class="">
                                            <h4 class="item-title">

                                                {{$item->email}}
                                            </h4>
                                        </a>
                                    </div>
                                </div>



                                <div class="item-col fixed pull-left item-col-title">
                                    <div class="item-heading">Discount value</div>
                                    <div>
                                        <a href="{{ URL('/admin/requests/' . $item->id ) }}" class="">
                                            <h4 class="item-title">

                                                {{$item->subject}}
                                            </h4>
                                        </a>
                                    </div>
                                </div>





                                <div class="item-col fixed pull-left item-col-title">
                                    <div class="item-heading">Course</div>
                                    <div>
                                        <a href="{{ URL('/admin/requests/' . $item->id ) }}" class="">
                                            <h4 class="item-title">

                                                {{$item->is_read==true? 'yes' : 'no'}}
                                            </h4>
                                        </a>
                                    </div>
                                </div>









                                <div class="item-col fixed pull-left item-col-title">
                                    <div class="item-heading">Created</div>
                                    <div>
                                        <a href="{{ URL('/admin/requests/' . $item->id ) }}" class="">
                                            <h4 class="item-title">
                                             @if( $item->created_at!='') {{$item->created_at->diffForHumans()}} @else
                                            {{$item->created_at}} @endif</h4>
                                        </a>
                                    </div>
                                </div>



                                <div class="item-col fixed item-col-actions-dropdown">
                                    <div class="item-actions-dropdown">
                                        <a class="item-actions-toggle-btn"> <span class="inactive">
                                        <i class="fa fa-cog"></i>
                                    </span> <span class="active">
                                    <i class="fa fa-chevron-circle-right"></i>
                                    </span> </a>
                                        <div class="item-actions-block">
                                            <ul class="item-actions-list">
                                                <li>
                                                    <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal{{$item->id}}">
                                                        <i class="fa fa-trash-o "></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="show" href="{{ URL('/admin/requests/' . $item->id ) }}"> <i class="fa fa-pencil"></i> </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <div class="modal fade" id="confirm-modal{{$item->id}}">
                            <form action="/admin/requests/delete/{{$item->id}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_locale_id" value="{{$item->id}}">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="modal-title"><i class="fa fa-warning"></i> Alert</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure want to do this?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" name="yes" value="1" >Yes</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                            </form>

                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    @endforeach
                </ul>
            </div>
            @include("admin.partials.pagination", ['model' => $requests ])

        @else
            <div class="card items">
                <ul class="item-list striped">
                    <li class="item item-list-header hidden-sm-down">
                        <div class="item-row item-title">No data found</div>
                    </li>

                </ul>

                <div>
        @endif
    </article>

@stop

@section('javascripts')


@stop
