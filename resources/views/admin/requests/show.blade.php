@extends('admin/layout')

@section('styles')

@stop

@section('content')
    <article class="content item-editor-page">
        @include('partials._form-errors')
        <div class="title-block">
            <h3 class="title"> Contact us message  <span class="sparkline bar" data-type="bar"></span> </h3>
        </div>



        <div class="card card-block">

            <ol class="breadcrumb">
                <li><a href="{{url('/')}}/admin">Home</a></li>
                <li><a href="{{url('/')}}/admin/requests">Job requests</a></li>
                <li class="active">show cv</li>
            </ol>






            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    name
                </label>
                <div class="col-sm-10">

                    {{$message->name}}
                </div>
            </div>



            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    email
                </label>
                <div class="col-sm-10">
                    {{$message->email}}
                </div>
            </div>




            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    subject
                </label>
                <div class="col-sm-10">
                    {{$message->subject}}
                </div>
            </div>





            <div class="form-group row">
                <label class="col-sm-2 form-control-label text-xs-right">
                    Cv
                </label>
                <div class="col-sm-10">
                   <a href="/files/{{$message->cv}}" target="_blank">show cv</a>
                </div>
            </div>






        </div>





    </article>

@stop

@section('javascripts')

    <script src="/assets/ckeditor/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'editor1' );
    </script>


@stop
