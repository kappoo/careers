@extends('layout.main')

@section('head')
	<li><a > تعديل البيانات الشخصية</a></li>
@stop

@section('main')
    <div class="col-sm-8">

        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <ul>
                            <?php echo implode('', $errors->all('<li class="error">:message</li>')) ;?>
                        </ul>
                    </div>
                @endif
            </div>
        </div>


    <?php echo  Form::model($user, array('class' => 'form-horizontal validate', 'method' => 'POST', 'route' => ['profile'])) ;?>

        <div class="well">

            <div class="form-group">
                <?php echo  Form::label('name', 'الاسم:', array('class'=>'col-md-2 control-label')) ;?>
                <div class="col-sm-10">
                    <?php echo  Form::text('name', $user->name, array('class'=>'form-control', 'placeholder'=>'الاسم','required')) ;?>
                </div>
            </div>

            <div class="form-group">
                <?php echo  Form::label('email', 'البريد الالكتروني:', array('class'=>'col-md-2 control-label')) ;?>
                <div class="col-sm-10">
                <?php echo  Form::email('email', $user->email, array('class'=>'form-control', 'placeholder'=>'البريد الالكتروني','required')) ;?>
                </div>
            </div>

            <div class="form-group">
                <?php echo  Form::label('password', 'كلمة المرور:', array('class'=>'col-md-2 control-label')) ;?>
                <div class="col-sm-10">
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#chPassword_Modal">{{t('change password')}}</a>
                </div>
            </div>




            <input type="hidden" value="{{Auth::guard('admin')->user()->id}}" name="profile">
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <?php echo  Form::submit('حفظ', array('class' => 'btn btn-primary')) ; ?>
                    <?php echo link_to('admin','إالغاء', array('class' => 'btn btn-default')) ; ?>
                </div>
            </div>

        </div>


    <?php echo  Form::close() ;?>

    <div class="modal fade" id="chPassword_Modal">
      <div class="modal-dialog">
        <?php echo  Form::open(['route'=>'change_password','id'=>'frm_status_model','class'=>'form-horizontal validate']) ;?>
            <div class="modal-content">
              <div class="modal-header modal-header-primary">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">تغيير كلمة المرور</h4>
              </div>
              <div class="modal-body">
                    <?php echo  Form::hidden('user_id',$user->id,['class'=>'user_id_modal']) ;?>
                    <div class="form-group">
                        <?php echo  Form::label('password', 'كلمة المرور الجديدة:', array('class'=>'col-md-3 control-label')) ;?>
                        <div class="col-md-9">
                          <?php echo  Form::password('new_password',array('class'=>'form-control', 'placeholder'=>'كلمة المرور الجديدة ...')) ;?>
                        </div>
                    </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                <button type="submit" class="btn btn-success status_btn">حفظ</button>
              </div>
            </div>
        <?php echo  Form::close() ;?>
      </div>
    </div>
</div>
@stop