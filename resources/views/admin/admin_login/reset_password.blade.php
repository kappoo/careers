@extends('layout.ajax')

@section('main')
	<div class="container margin50">
		<div class="row">
			<div class="col-lg-4 col-lg-push-4 col-md-6 col-md-push-3 col-sm-8 col-sm-push-2">

				@if(Session::has('message'))
		            <div class="alert alert-{{Session::get('alert')}} progress-bar-striped">
		            {{ Session::get('message') }}
		            </div>
		        @endif

		        @if ($errors->any())
                	<div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">
                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                	    <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                	</div>
                @endif

				<div class="panel">
					<div class="panel-heading"><h1 class="text-danger text-center">Reset Password</h1></div>
					<div class="panel-body">
						{{ Form::open(['route'=>'reset_password','class'=>'content-box']) }}
							<div class="form-group">
						        {{ Form::label('email', 'Enter your email to reset your password',['class'=>'text-danger']) }}
						        <div class="input-group">
							        {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email Address ...')) }}
							        <label for="uLogin" class="input-group-addon"><i class="fa fa-user"></i></label>
						        </div>
						    </div>

						    <div class="form-group">
						        {{ Form::submit('Submit', ['class'=>'btn btn-danger col-md-12']) }}
						    </div>

						    <div class="form-group">
						    	<a href="{{route('login')}}" class="btn col-sm-12">Back to Login</a>
						    </div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>

	<style type="text/css">
	.margin50 { margin-top:50px;}
	</style>
@stop