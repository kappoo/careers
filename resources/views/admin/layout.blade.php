<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Careers </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="/admin_assets/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!-- Theme initialization -->
    <style>

        .shadow{
            background-color: #e3e3e3;
        }
        .breadcrumb{padding:8px 15px;margin-bottom:20px;list-style:none;background-color:#f5f5f5;border-radius:4px}.breadcrumb>li{display:inline-block}.breadcrumb>li+li:before{padding:0 5px;color:#ccc;content:"/\00a0"}.breadcrumb>.active{color:#777}
    </style>
    @yield('styles')
    <script>
        var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
        var themeName = themeSettings.themeName || '';
        if (themeName)
        {
            document.write('<link rel="stylesheet" id="theme-style" href="/admin_assets/css/app-' + themeName + '.css">');
        }
        else
        {
            document.write('<link rel="stylesheet" id="theme-style" href="/admin_assets/css/app.css">');
        }
    </script>
</head>

<body>
<div class="main-wrapper">
    <div class="app" id="app">


        <!-- BEGIN Header -->
        @include('admin/partials/header')
        <!-- END Header -->

        <!-- BEGIN Sidebar -->
        @include('admin/partials/sidebar')
        <!-- END Sidebar -->






    <!-- BEGIN CONTENT -->
        @yield('content')
        <!-- END CONTENT -->






    <!-- BEGIN Footer -->
        @include('admin/partials/footer')
        <!-- END Footer -->


    </div>
</div>


<script src="/admin_assets/js/vendor.js"></script>
<script src="/admin_assets/js/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $('select').select2();
</script>
@yield('javascripts')
</body>

</html>