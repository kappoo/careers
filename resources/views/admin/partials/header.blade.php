
<header class="header">
    <div class="header-block header-block-collapse hidden-lg-up"> <button class="collapse-btn" id="sidebar-collapse-btn">
            <i class="fa fa-bars"></i>
        </button> </div>
    <div class="header-block header-block-search hidden-sm-down">

    </div>
    <div class="header-block header-block-buttons">

    </div>
    <div class="header-block header-block-nav">
        <ul class="nav-profile">
            <li class="notifications new">
                <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup>

                        <span class="counter">0</span>
                    </sup> </a>
                <div class="dropdown-menu notifications-dropdown-menu">
                    <ul class="notifications-container">

                    </ul>
                    <footer>
                        <ul>
                            <li> <a href="{{url('/')}}/admin/notifications">
                                    View All
                                </a> </li>
                        </ul>
                    </footer>
                </div>
            </li>

            <li class="profile dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                    <div class="img" style="background-image: url({{ URL("/images/profiles/") }})"
                    > </div> <span class="name">

                       {{Auth::user()->name}}
    			    </span> </a>
                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item" href="{{url("admin/users/". Auth::user()->id . "/profiles")}}"> <i class="fa fa-user icon"></i> Profile </a>
                    <a class="dropdown-item" href="{{url("admin/notifications")}}"> <i class="fa fa-bell icon"></i> Notifications </a>
                    <a class="dropdown-item" href="{{url("admin/users/". Auth::user()->id . "/systemsettings")}}"> <i class="fa fa-gear icon"></i> Settings </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('/')}}/admin/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
                </div>
            </li>
        </ul>
    </div>
</header>
