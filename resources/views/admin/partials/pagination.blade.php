<nav class="text-xs-right">

    <?php
    // config

        if(!isset($limit)) $limit=12;
    $link_limit = $limit; // maximum number of links (a little bit inaccurate, but will be ok for now)
    ?>

    <?php

        $fullLink='';
        foreach (request()->all() as $key=>$value)
            {
                if($key!='page'){
                    $fullLink=$fullLink."&$key=$value";

                }
            }


    ?>
    @if ($model->lastPage() > 1)
        <ul class="pagination">
            <li class="{{ ($model->currentPage() == 1) ? ' disabled' : '' }} page-item">
                <a  class="page-link" href="{{ $model->url(1) }}{{$fullLink}}">First</a>
            </li>
            @for ($i = 1; $i <= $model->lastPage(); $i++)
                <?php
                $half_total_links = floor($link_limit / 2);
                $from = $model->currentPage() - $half_total_links;
                $to = $model->currentPage() + $half_total_links;
                if ($model->currentPage() < $half_total_links) {
                    $to += $half_total_links - $model->currentPage();
                }
                if ($model->lastPage() - $model->currentPage() < $half_total_links) {
                    $from -= $half_total_links - ($model->lastPage() - $model->currentPage()) - 1;
                }
                ?>
                @if ($from < $i && $i < $to)
                    <li class="{{ ($model->currentPage() == $i) ? ' active' : '' }} page-item">
                        <a class="page-link" href="{{ $model->url($i) }}{{$fullLink}}">{{ $i }}</a>
                    </li>
                @endif
            @endfor
            <li class="{{ ($model->currentPage() == $model->lastPage()) ? ' disabled' : '' }} page-item">
                <a class="page-link" href="{{ $model->url($model->lastPage()) }}{{$fullLink}}">Last</a>
            </li>
        </ul>
    @endif

</nav>