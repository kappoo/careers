<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use \App\User;


Route::get('/first-run',function (){

    if(!User::count())
    {
        User::create(['email'=>'admin@email.com',
            'name'=>'beetleware',
            'password'=>bcrypt('123456'),
            'is_admin'=>1]);
    }
});

Route::group(['middleware'=>'web'],function(){


    Route::get('/', function () {
        return view('welcome');
    });

    Route::post('/post-cv','HomeController@postCv');
});




Route::group(['middleware'=>'web','namespace'=>'Admin','prefix'=>'admin'],function (){

    Route::get('/login', array('as' => 'login', 'uses' => 'AdminLoginController@login'));
    Route::post( '/login', [ 'as' => 'login_post', 'uses' => 'AdminLoginController@login' ] );
    Route::get( '/logout', array('as' => 'logout', 'uses' => 'AdminLoginController@logout') );


});






Route::group(['middleware' => ['web','CheckAdmin'],  'as' => 'admin', 'namespace' => 'Admin' ,'prefix' => 'admin' ] ,function () {


    Route::get('/', [
        'as' => 'index',
        'uses' => 'DashboardController@index'
    ]);
    Route::get('/index', [
        'as' => 'index',
        'uses' => 'DashboardController@index'
    ]);


    Route::resource('requests',"RequestsController");
    Route::post('/requests/delete/{id}', 'RequestsController@destroy')->name("delete");
});
